import React, { useContext } from 'react'
import { TaskListContext } from '../contexts/TaskListContext'

import { connect } from 'react-redux'
import { removeTaskFromList } from '../store/action'

const Task = ({ task, removeTaskFromList }) => {
  const {findItem } = useContext(TaskListContext)
 const removeTask = ( task) =>{
  removeTaskFromList(task)
 }
 console.log("pr_task",task.hb);
  
  return (
    
  <>
    <tr>
      <td>{task.pr_name}</td>
      <td>{task.title}</td>
      <td>{task.age}</td>
      <td>{task.gender}</td>
      <td>{task.date}</td>
      <td>{task.hb.toString()}</td>
      <td>{task.status}</td>
      <td><button
      className="btn-delete task-btn"
      onClick={() => removeTask(task.id)}
    >
      <i className="fas fa-trash-alt"></i>
    </button>
    {' '}
    <button className="btn-edit task-btn" onClick={() => findItem(task.id)}>
      <i className="fas fa-pen"></i>
    </button>
    </td>
    </tr>
  </>
  )
}

const mapStateToProps = state => {
  return {
    tasks: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
    removeTaskFromList: task => dispatch(removeTaskFromList(task)),
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Task)
