import React, { useState, useContext, useEffect } from 'react'
import uuid from 'uuid'
import { TaskListContext } from '../contexts/TaskListContext'
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Checkbox from '@material-ui/core/Checkbox';
import FormGroup from '@material-ui/core/FormGroup';
//redux implementation
import { connect } from 'react-redux'
import { addTaskToList } from '../store/action'
import { clearTaskFromList } from '../store/action'



const TaskForm = ({addTaskToList,clearTaskFromList}) => {
  const { editTask, editItem } = useContext(TaskListContext)
  const [pr_name, setName] = useState('')
  const [date, setDate] = useState('')
  const [title, setTitle] = useState('')
  const [age, setAge] = useState('')
  const [gender, setGender] = useState('')
  const [status, setStatus] = useState('Active')
  const [hobby, setHobby] = useState({
    sports: false,
    reading: false,
    music: false,
  });

  const handleChange = e => {
    setTitle(e.target.value)
  }
  const handleChangeAge = e => {
    setAge(e.target.value)
  }
  const handleChangeGender = e => {
    setGender(e.target.value)
    console.log(gender);
  }
  const handleChangeName = e => {
    setName(e.target.value)
  }
  const handleChangeDate = e => {
    setDate(e.target.value)
  }
  const handleChangeStatus = e => {
    setStatus(e.target.value)
  }
  const handleChangeHobby = (event) => {
    setHobby({ ...hobby, [event.target.name]: event.target.checked });
  };
  let pr_hobby = Object.keys(hobby).filter(k => hobby[k])
  console.log(pr_hobby);
  useEffect(() => {
    if (editItem) {
      setTitle(editItem.title)
      setAge(editItem.age)
      setGender(editItem.gender)
      setName(editItem.pr_name)
      setDate(editItem.date)
      setStatus(editItem.status)
      setHobby(editItem.hobby)
      console.log(editItem)
    } else {
      setTitle('')
      setAge('')
      setGender('')
      setStatus('Active')
      setName('')
      setHobby({
        sports: false,
        reading: false,
        music: false,
      })
    }
  }, [editItem])


  const addTaskk = (event) =>{
    event.preventDefault();
    if (!editItem) {
      addTaskToList(
        {
          id: uuid(),
          pr_name :pr_name,
          date :date,
          title :title,
          age :age,
          gender :gender,
          status :status,
          hobby :hobby,
          hb:pr_hobby,
        }
      );
    }else {
      editTask(pr_name,date,title,age,gender,status,hobby,pr_hobby, editItem.id)
    }
  }
  const clearTaskk = (event) =>{
    event.preventDefault();
    clearTaskFromList()
  }
  return (
    <form onSubmit={addTaskk} className="form">
      
      <input
        pattern="[a-zA-Z]{0,15}\s?([a-zA-Z]{0,9})?"
        type="text"
        placeholder="Name"
        value={pr_name}
        onChange={handleChangeName}
        required
        className="task-input"
      />
      <input
        type="text"
        placeholder="Add Task..."
        value={title}
        onChange={handleChange}
        required
        className="task-input"
      />
      <input
        min="18"
        max="55"
        type="number"
        placeholder="age between 18 to 55 Only..."
        value={age}
        onChange={handleChangeAge}
        required
        className="task-input"
      />
      <input
        type="date"
        placeholder="Date."
        value={date}
        onChange={handleChangeDate}
        required
        className="task-input"
      />
      <Select style = {{marginTop : "10px"}}
        labelId="demo-simple-select-label"
        id="demo-simple-select"
        value={status}
        onChange={handleChangeStatus}
      >
        <MenuItem value={"Active"}>Active</MenuItem>
        <MenuItem value={"Inactive"}>Inactive</MenuItem>
      </Select>
      <RadioGroup row aria-label="gender" name="gender1" value={gender} onChange={handleChangeGender}>
        <FormControlLabel value="female" control={<Radio />} label="Female" />
        <FormControlLabel value="male" control={<Radio />} label="Male" />
      </RadioGroup>
      <FormGroup aria-label="position" row>
        <FormControlLabel
            control={<Checkbox checked={hobby.sports} onChange={handleChangeHobby} name="sports" />}
            label="Sports"
          />
          <FormControlLabel
            control={<Checkbox checked={hobby.reading} onChange={handleChangeHobby} name="reading" />}
            label="Reading"
          />
          <FormControlLabel
            control={<Checkbox checked={hobby.music} onChange={handleChangeHobby} name="music" />}
            label="Music"
          />
      </FormGroup>
      
      <div className="buttons">
        <button type="submit" className="btn add-task-btn">
          {editItem ? 'Edit Task' : 'Add Task'}
        </button>
        <button className="btn clear-btn" onClick={clearTaskk}>
          Clear
        </button>
      </div>
    </form>
  )
}

const mapStateToProps = state => {
  return {
    tasks: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addTaskToList: task => dispatch(addTaskToList(task)),
    clearTaskFromList: () => dispatch(clearTaskFromList()),
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(TaskForm)
