import React, { useContext } from "react";
import { TaskListContext } from "../contexts/TaskListContext";
import Task from "./Task";

const TaskList = () => {
  const { tasks } = useContext(TaskListContext);

  return (
    <div>
      {tasks.length ? (
        <ul className="list">
            <table className="table table-striped">
              <tr>
                <th>Name</th>
                <th>Tasks</th>
                <th>Age</th>
                <th>Gender</th>
                <th>Date</th>
                <th>Hobby</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
              
          {tasks.map(task => {
            return (
              <Task task={task} key={task.id} />
              )
            })}
              </table>
        </ul>
      ) : (
        <div className="no-tasks">No Tasks</div>
      )}
    </div>
  );
};

export default TaskList;



