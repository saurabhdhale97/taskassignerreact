import React, { createContext, useState, useEffect } from 'react'

export const TaskListContext = createContext()

const TaskListContextProvider = props => {
  const initialState = JSON.parse(localStorage.getItem('tasks')) || []

  const [tasks, setTasks] = useState(initialState)

  useEffect(() => {
    localStorage.setItem('tasks', JSON.stringify(tasks))
  }, [tasks])

  const [editItem, setEditItem] = useState(null)

  
  // Find task
  const findItem = id => {
    const item = tasks.find(task => task.id === id)

    setEditItem(item)
  }

  // Edit task
  const editTask = (pr_name,date,title,age,gender,status,hobby,hb, id) => {
    const newTasks = tasks.map(task => (task.id === id ? { pr_name,date,title,age,gender,status,hobby,hb, id } : task))

    console.log(newTasks)

    setTasks(newTasks)
    setEditItem(null)
    window.location.reload()
  }

  return (
    <TaskListContext.Provider
      value={{
        tasks,
        findItem,
        editTask,
        editItem
      }}
    >
      {props.children}
    </TaskListContext.Provider>
  )
}

export default TaskListContextProvider
