import React from 'react'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import ReactDOM from 'react-dom'
import App from './components/App'
import reduxThunk from 'redux-thunk'
import taskListReducer from './store/reducers';


// reduxThunk allows exection of asyn code in action creators
const store = createStore(taskListReducer, applyMiddleware(reduxThunk));
ReactDOM.render(
    <Provider store={store}>{}
        <App />
    </Provider>
    , document.getElementById('root'))
