import { ADD_TASK_TO_LIST, REMOVE_TASK_FROM_LIST,CLEAR_TASK_FROM_LIST } from './action'
var tasks = JSON.parse(localStorage.getItem('tasks')) || []
const taskListReducer = (state = tasks, action) => {
  switch (action.type) {

    case ADD_TASK_TO_LIST:
      tasks.push(action.payload);
      localStorage.setItem('tasks', JSON.stringify(tasks))
      window.location.reload()
      return  [...tasks]

    case REMOVE_TASK_FROM_LIST:
      const findItem = id => {
        const item = tasks.find(task => task.id === id)
        console.log("item_to_process",item);
        tasks = tasks.filter(task => task.id !== id)
        localStorage.setItem('tasks', JSON.stringify(tasks))
        window.location.reload()
      }
      findItem(action.payload)
      return  [...tasks]

    case CLEAR_TASK_FROM_LIST:
      tasks  = []
      localStorage.setItem('tasks', JSON.stringify(tasks))
      return  [...tasks] 
    default:
      return tasks
  }
}

export default taskListReducer;