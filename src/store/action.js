export const ADD_TASK_TO_LIST = 'ADD_TASK_TO_LIST'
export const REMOVE_TASK_FROM_LIST = 'REMOVE_TASK_FROM_LIST'
export const CLEAR_TASK_FROM_LIST = 'CLEAR_TASK_FROM_LIST'

export const addTaskToList = task => {
  console.log(task);
  return dispatch => {
    setTimeout(() => {
      dispatch({
        type: ADD_TASK_TO_LIST,
        payload: task,
      })
    }, 700)
  }
}

export const removeTaskFromList = taskId => {
  return dispatch => {
    setTimeout(() => {
      dispatch({
        type: REMOVE_TASK_FROM_LIST,
        payload: taskId,
      })
    }, 700)
  }
}

export const clearTaskFromList = taskId => {
  return dispatch => {
    setTimeout(() => {
      dispatch({
        type: CLEAR_TASK_FROM_LIST,
      })
    }, 700)
  }
}